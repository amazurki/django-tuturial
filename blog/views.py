from django.shortcuts import render

posts = [
    {
        'author': 'Przemek',
        'title': 'zolw',
        'content': 'zly zolw',
        'date_posted': '05.05.2021'
    },
    {
        'author': 'Przemek2',
        'title': 'zolw2',
        'content': 'zly zolw2',
        'date_posted': '05.05.20212'
    }
]

def home(request):
    context = {
        'posts': posts
    }
    return render(request, 'blog/home.html', context)

def about(request):
    return render(request, 'blog/about.html', {'title': 'About'})
